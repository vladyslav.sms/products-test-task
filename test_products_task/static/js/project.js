/* Project specific Javascript goes here. */


$('.js_product_like').click(function () {
    const button = $(this);
    const product_id = button.data('product_id');
    $.post("/products/"+product_id+'/like/', function (data) {
        if(data.status) {
            $('.js_likes_count').text(data.count);
            button.remove();
        } else {
            alert(data.message);
        }
    }, 'json');
})

$('.js_comment_form').submit(function (e) {
    e.preventDefault();
    const $form = $(this);
    const product_id = $('[name="product_id"]', $form).val();
    $.post("/products/"+product_id+'/comment/', $form.serialize(), function (data) {
        if(data.status) {
            $('#comments').html(data.html);
            $form[0].reset();
        } else {
            alert(data.message);
        }
    }, 'json');
    return false;
})