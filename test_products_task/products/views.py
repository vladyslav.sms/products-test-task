from collections import OrderedDict
from datetime import datetime, timedelta

from braces.views import JSONResponseMixin, AjaxResponseMixin
from django.contrib import messages
from django.db.models import Sum, Case, When, IntegerField, Count, F, Q
from django.db.models.functions import Coalesce
from django.http import Http404, request, HttpResponseRedirect, HttpResponse, JsonResponse, HttpResponseBadRequest
from django.shortcuts import redirect, get_object_or_404, render
from django.template.loader import render_to_string
from django.urls import reverse_lazy, reverse
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, DetailView, FormView, TemplateView
from django.views.generic.list import MultipleObjectMixin

from test_products_task.common.mixins import ActiveTabMixin
from test_products_task.common.utils import get_ip_from_request
from test_products_task.products.forms import LikeForm, CommentForm
from test_products_task.products.models import Category, Product, Like, Comment

from django.core.paginator import Paginator
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger


class CategoryListView(ActiveTabMixin, ListView):
    model = Category
    active_tab = 'category_list'

    # paginate_by = 2

    def get_ordered_grade_info(self):
        return []

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['grade_info'] = self.get_ordered_grade_info()
        return context


class CategoryDetailView(DetailView):
    model = Category
    paginate_by = 3
    template_name = 'products/category_detail.html'
    slug_url_kwarg = 'category_slug'
    context_object_name = 'category_detail'

    PARAM_FOLLOWING = 'following'
    PARAM_PRICE_FROM = 'price_from'
    PARAM_PRICE_TO = 'price_to'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        product_list = Product.objects.filter(category=self.object)
        paginator = Paginator(product_list, 2)  # Show 25 products per page
        page = self.request.GET.get('page')
        try:
            products = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            products = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            products = paginator.page(paginator.num_pages)
        context['products'] = products
        context['form'] = CommentForm()
        return context


class ProductDetailView(DetailView):
    model = Product
    slug_url_kwarg = 'product_slug'
    category = None

    def get(self, request, *args, **kwargs):
        category_slug = kwargs['category_slug']
        try:
            self.category = Category.objects.get(slug=category_slug)
        except Category.DoesNotExist:
            raise Http404
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        category_slug = kwargs['category_slug']
        try:
            self.category = Category.objects.get(slug=category_slug)
        except Category.DoesNotExist:
            raise Http404
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        ip = None
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
        else:
            ip = get_ip_from_request(self.request)
        try:
            Like.objects.get(user=user, ip=ip, product=context['product'])
            context['liked'] = True
        except:
            context['liked'] = False
        return context


class LikeToggleView(AjaxResponseMixin, JSONResponseMixin, FormView):
    http_method_names = ('post',)
    form_class = LikeForm
    product = None

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        product_id = kwargs['product_pk']
        try:
            self.product = Product.objects.get(id=product_id)
        except Product.DoesNotExist:
            raise Http404()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        res = {"status": False, "message": "Error!"}
        ip = None
        user = None
        if request.user.is_authenticated:
            user = request.user
        else:
            ip = get_ip_from_request(request)
        try:
            Like.objects.get(user=user, ip=ip, product=self.product)
            res['message'] = "You already liked this product!"
            return HttpResponse(JsonResponse(res))
        except:
            pass

        like = Like.objects.create(user=user, ip=ip, product=self.product)
        like.save()
        res['status'] = True
        res['count'] = self.product.likes.count()
        res['message'] = "You have successfully liked this product!"
        return HttpResponse(JsonResponse(res))


class AddToCartView(AjaxResponseMixin, JSONResponseMixin, FormView):
    http_method_names = ('post',)
    success_url = reverse_lazy('products:cart')

    def post(self, request, *args, **kwargs):
        raise NotImplementedError


class CartView(ActiveTabMixin, TemplateView):
    active_tab = 'cart'
    template_name = 'products/cart.html'


class CommentToggleView(AjaxResponseMixin, JSONResponseMixin, FormView):
    http_method_names = ('post',)
    form_class = CommentForm
    product = None

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        product_id = kwargs['product_pk']
        try:
            self.product = Product.objects.get(id=product_id)
        except Product.DoesNotExist:
            raise Http404()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):

        res = {"status": False, "message": "Error!"}
        form = CommentForm(request.POST)

        if not form.is_valid():
            res['message'] = form.errors['text']
            return HttpResponse(JsonResponse(res))
        form = form.save(commit=False)
        form.product = self.product

        if request.user.is_authenticated:
            form.user = request.user
        else:
            form.ip = get_ip_from_request(request)
        form.save()

        html = render_to_string('products/comment_list.html', {'product': self.product})
        res['status'] = True
        res['html'] = html
        res['message'] = "You have successfully comment this product!"
        return HttpResponse(JsonResponse(res))
