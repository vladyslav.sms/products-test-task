from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import Count
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from model_utils import Choices
from model_utils.models import TimeStampedModel
from test_products_task.common.utils import generate_slug
from django.template.defaultfilters import slugify
from datetime import datetime, timedelta
from time import time
from config import settings


class Category(models.Model):
    name = models.CharField(_('Name'), max_length=200)
    slug = models.SlugField(_('Slug'), unique=True, db_index=True, blank=True)

    class Meta:
        ordering = ('name',)

    PARAMS = Choices(
        ('following', 'following'),
        ('price_to', 'price_to'),
        ('price_from', 'price_from'),
    )

    def get_absolute_url(self):
        return reverse('products:category_detail', args=[str(self.slug)])

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = generate_slug(self.name)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name

    def products_count(self, obj):
        return obj.products_count

    products_count.short_description = 'products count'
    products_count.admin_order_field = 'products_count'

    def get_products_count(self):
        return Product.objects.filter(category=self).count()


class Product(TimeStampedModel):
    GRADE_CHOICES = Choices(
        ('base', 'base', _('Base')),
        ('standard', 'standard', _('Standard')),
        ('premium', 'premium', _('Premium')),
    )

    name = models.CharField(_('Name'), max_length=200)
    slug = models.SlugField(_('Slug'), unique=True, db_index=True, blank=True)
    price = models.DecimalField(_('Price'), decimal_places=2, max_digits=9)
    description = models.TextField(_('Description'), blank=True)
    category = models.ForeignKey(Category, related_name='products')

    image = models.ImageField(_("Image"), upload_to='test_products_task/static/images/products_images',
                              db_column='Images',
                              null=True,
                              blank=True)

    class Meta:
        ordering = ('-created',)

    def get_absolute_url(self):
        return reverse('products:product_detail', kwargs={'category_slug': self.category.slug,
                                                          'product_slug': self.slug})

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = generate_slug(self.name)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name

    def image_tag(self):
        return mark_safe('<img src="%s" width="250" height="191" />' % (
            self.image.url))  # Get Image url

    image_tag.short_description = 'thumbnail'


class Like(TimeStampedModel):
    product = models.ForeignKey(Product, related_name='likes')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, related_name='likes')
    ip = models.GenericIPAddressField(blank=True, null=True)

    class Meta:
        unique_together = (('product', 'user'), ('product', 'ip'))

    def get_like_url(self):
        return reverse("products:like_toggle", kwargs={"slug": self.slug})

    def __str__(self):
        return '{} from {}'.format(self.product, self.user or self.ip)


class Comment(TimeStampedModel):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='comments')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, related_name='comments')
    ip = models.GenericIPAddressField(blank=True, null=True)
    text = models.TextField(_('Comment'), max_length=500)
    created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-created_on']

    def __str__(self):
        return 'Comment {} by {}'.format(self.text, self.user or self.ip)
