from django.contrib import admin
from django.db.models import Count
from django.utils.safestring import mark_safe

from test_products_task.products.models import Category, Product, Like, Comment


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    readonly_fields = ('image_tag',)
    list_filter = ('category__name',)


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'products_count')

    def get_queryset(self, request):
        return Category.objects.annotate(products_count=Count('products'))

@admin.register(Like)
class LikeAdmin(admin.ModelAdmin):
    list_display = ('product', 'user','ip',)

@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ('product', 'user','ip','created_on',)
    search_fields = ('product', 'user', 'text')
