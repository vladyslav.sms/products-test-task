from django.template.defaultfilters import slugify
from time import time


def get_ip_from_request(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def generate_slug(slug: object) -> object:
    new_slug = slugify(slug)
    return new_slug + '-' + str(int(time()))
